import com.devcamp.s50.Task5650.Account;

public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("KSJ483", "Henry");
        Account account2 = new Account("JFN543", "Harris", 1000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        account1.credit(2000);
        account2.credit(3000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        account1.debit(1000);
        account2.debit(5000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        account1.transferTo(account2, 500);
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        account2.transferTo(account1, 2000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
    }
}
