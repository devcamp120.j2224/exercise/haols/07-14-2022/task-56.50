package com.devcamp.s50.Task5650;

public class Account {
    private String id = "none";
    private String name = "none";
    private int balance = 0;

    public Account() {
        super();
    }

    public Account(String id, String name) {
        this.name = name;
        this.id = id;
    }

    public Account(String id, String name, int balance) {
        this(id, name);
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public int credit(int amount) {
        this.balance = balance + amount;
        return this.balance;
    }

    public int debit(int amount) {
        if (amount <= balance) {
            this.balance = balance - amount;
        } else {
            System.out.println("Amount exceeded balance");
        }
        return balance;
    }

    public int transferTo(Account another, int amount) {
        if (amount <= balance) {
            this.debit(amount);
            another.credit(amount);
        } else {
            System.out.println("Amount exceeded balance");
        }
        return balance;
    }

    @Override
    public String toString() {
        return "Account [id=" + id
                + ", name=" + name
                + ", balance=" + balance + "]";
    }
}
